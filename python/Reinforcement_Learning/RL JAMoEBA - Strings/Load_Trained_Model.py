# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:39:29 2020

@author: 17088
"""

from stable_baselines import PPO1
import Strings_Environment as Strings

data_collect = False # Collects data and reports it in .csv files for later investigation.
plot = False # Plots your data immediately. Will NOT work if data_collect=False
render = True # Do you want to see the simulation?
POV_Ray = False

env = Strings.Strings(data_collect=data_collect, experiment_name='Experiment_60a_Interior_Safe', plot=plot, POV_Ray = POV_Ray)
model = PPO1.load('ppo1_Strings_CustomPolicy_Experiment_60a.zip')

obs=env.reset()

for i in range(2500):
    action, _states = model.predict(obs, deterministic=False)
    obs, reward, done, info = env.step(action)
    
    if render:
        env.render()
    
    if done:
        break

if data_collect:
    env.data_export() # Export the data from the simulation
env.close()