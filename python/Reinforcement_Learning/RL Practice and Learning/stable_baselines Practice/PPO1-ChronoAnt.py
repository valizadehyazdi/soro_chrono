# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:20:55 2020

@author: elopez8
"""
import ChronoGymAnt # gym-chrono version of the ant

from stable_baselines.common.policies import MlpPolicy #Learn what this is
from stable_baselines import PPO1

env = ChronoGymAnt.ChronoAnt()
model = PPO1(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=10000)
model.save('ppo1_ChronoAnt')
env.close()